import React, { useState } from 'react';
import './App.css';
import Person from './Person/Person.js';

const app = props => {
    const [personsState, setPersonsState] = useState({
      persons: [
        {name: "Levi", age: 22},
        {name: "Geri", age: 21},
        {name: "Kristof", age: 23}
      ],
      otherState:'Some other Value'
    });
    
    const switchNameHandler = () => {
      // console.log("Clicked");
      // DONT DO THIS: this.state.persons[0].name="Levente";
      setPersonsState({
        persons: [
          {name: "Max", age: 23},
          {name: "Gergo", age: 21},
          {name: "Titi", age: 27}
        ]
      });
    };
    
    return (
      <div className="App">
        <h1>Hi Im a React app :)</h1>
        <button onClick ={switchNameHandler}>Switch Name</button>
        <Person 
          name={personsState.persons[0].name} 
          age={personsState.persons[0].age}/>
        <Person 
          name={personsState.persons[1].name} 
          age={personsState.persons[1].age}
          click={switchNameHandler}/>
        <Person 
          name={personsState.persons[2].name} 
          age={personsState.persons[2].age}/>

      </div>
    );
    // return React.createElement('div', {className:'App'}, 
    //             React.createElement('h1', null, 'It wooorks!'));
  };


export default app;


